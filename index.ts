import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetDisplayPagingComponent } from './src/base-widget.component';
export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
	BaseWidgetDisplayPagingComponent
  ],
  exports: [
	BaseWidgetDisplayPagingComponent
  ]
})
export class DisplayPagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DisplayPagingModule,
      providers: []
    };
  }
}
