import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-displaypaging',
  template : `
<div class="container">
    <div class="col-md-12">
        <div class="col-md-3" *ngFor="let display of DisplayPaging" style="border: 1px ;
    border-style: outset;
    margin: 2px 2px 8px 8px;">
            <div class="col-md-4">
                <img src="./src/assets/icon.png" width="90px" height="80px" alt="">
            </div>
            <div class="col-md-8">
                <h4>{{display.display_name}}</h4>
                <p>{{display.counter}}  News</p>
            </div>
        </div>
    </div>
</div>
`
})
export class BaseWidgetDisplayPagingComponent implements OnInit {
public DisplayPaging: Array<any> = [
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	}
];
  constructor() { }

  ngOnInit() {
  }

}
